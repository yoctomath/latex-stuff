% author : sylcha
% licence : Creative Commons Attribution-NonCommercial-ShareAlike 4.0
%           https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
\NeedsTeXFormat{LaTeX2e}%
\ProvidesClass{simple}[2024/11/06 v0.1]%
\LoadClass[a4paper,10pt,svgnames]{article}%
% options
\RequirePackage{ProfCollege}%
\RequirePackage[french]{babel}%
% mise en page
\RequirePackage{geometry}%
\geometry{a4paper,bottom=1.5cm,top=3cm,left=2cm,right=2cm,nohead,nofoot}
% couleurs
\RequirePackage{xcolor}%
\RequirePackage{couleurs}%  
% tableaux
\RequirePackage{tabularray}%
\UseTblrLibrary{varwidth}%
% fontes
\RequirePackage[%
  % math={Noto Sans Math},%
  % title={Source Sans Pro Black},%
  main={Source Sans Pro}%
]{fontes}%
% listes
\RequirePackage{enumitem}%
\setlist[1]{labelindent=\parindent}%
\NewDocumentCommand{\SetListsColor}{ O{colorPrim} }{
  \setlist[enumerate,1]{%
    label   = {\textcolor{#1}{\textbf{\arabic*. }}},%
    ref     = \arabic*%
    left    = 0pt .. \parindent,%
    labelindent = \parindent,%
    align   = left,%
    leftmargin = *}%
  \setlist[enumerate,2]{%
    label       = \textcolor{#1}{\textbf{\alph*)} },%
    ref         = \theenumi.\emph{\alph*},%
    leftmargin  = 1em}%
  \setlist[itemize]{%
    label   = \raisebox{0.2ex}{\textcolor{#1}{\tiny \faCircle}},%
    leftmargin = 15pt%
  }%
  \setlist[itemize,2]{%
    label   = \raisebox{0.2ex}{\textcolor{#1}{\tiny \faMinus}},%
    leftmargin = 15pt%
  }%
}
\SetListsColor
% liens
\RequirePackage{hyperref}
\hypersetup{
bookmarks=true,         % show bookmarks bar?
unicode=false,          % non-Latin characters in Acrobat’s bookmarks
pdftoolbar=true,        % show Acrobat’s toolbar?
pdfmenubar=true,        % show Acrobat’s menu?
pdffitwindow=false,     % window fit to page when opened
pdfstartview={FitH},    % fits the width of the page to the window
pdftitle={},    % title
pdfauthor={Sylcha},     % author
pdfsubject={},   % subject of the document
pdfcreator={},   % creator of the document
pdfproducer={}, % producer of the document
pdfkeywords={keyword1, key2, key3}, % list of keywords
pdfnewwindow=true,      % links in new PDF window
colorlinks=true,       % false: boxed links; true: colored links
linkcolor=CornflowerBlue,          % color of internal links (change box color with linkbordercolor)
citecolor=green,        % color of links to bibliography
filecolor=magenta,      % color of file links
urlcolor=colorTextLight           % color of external links
}
% 
% Divers
% 
% 
% node[cross] draw a cross
\usetikzlibrary{shapes.misc}
\tikzset{cross/.style={cross out, draw=black, minimum size=2*(#1-\pgflinewidth), inner sep=0pt, outer sep=0pt},
  %default radius will be 1pt.
  cross/.default={1pt}} %[cross=2pt] for size, [cross, rotate=50] to rotate...
% pas d'indentation
\setlength{\parindent}{0pt}%
% icones
\RequirePackage{fontawesome5}%
% highlight text
\RequirePackage{soul}%
% touches du clavier
\RequirePackage[os=win]{menukeys}