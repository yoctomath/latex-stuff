# Matériel personnel

## Intention

Je partage ici des packages et des classes que j'utilise au quotidien dans la production de documents pour les élèves. Les document basés sur ces packages (ou classes) doivent être compilés avec [`lualatex`](https://www.luatex.org/)

<i class="fas fa-exclamation-triangle" aria-hidden="true"></i> Ces packages fonctionnent dans mon environnement $\LaTeX$ de travail mais ils n'ont pas vocation à être universels. Mais libre à tout un chacun d'en modifier le code pour l'adapter à ses besoins.

## Packages

### Package `fontes`

Utilisant [`fontspec`](https://www.ctan.org/pkg/fontspec), j'ai trouvé commode de regrouper dans un package la déclaration des fontes que j'utilise dans mes documents. Je n'ai quasiment pas recours aux fontes $\TeX$ traditionnelles mais très majoritairement à des fontes `ttf` installées sur ma machine.

Grâce à des options passées au package, je peux choisir quelle fonte je souhaite utiliser.

| Options | Fonte | Usage | Défaut |
|-------- | ---- | ----- | ------ |
| `main` | sans serif pour tout le document | par défaut | [Dinish](https://github.com/playbeing/dinish) |
| `mono` | largeur fixe | par défaut (`\texttt{...}` ou `{\ttfamily ...}`) | [Iosevka](https://github.com/be5invis/Iosevka) |
| `math` | pour les expressions mathématiques | par défaut (`$...$` ou `\[...\]`) | [GFS Neohellenic Math](https://ctan.org/pkg/gfsneohellenicmath)|
|`condensed`|sans serif mais condensé | `{\condensedfont ...}` | [League Gothic](https://fonts.adobe.com/fonts/league-gothic)|
|`title`| sans serif grasse pour les titre | `{\titlefont ...}` | [Barlow Condensed Black](https://github.com/jpt/barlow)|
|`cursive`| manuscrite | `{\cursivefont ...}` | [Nickainley](https://www.fontfabric.com/fonts/nickainley/)|

On peut évidemment changer ces options (c'est le but !) ainsi par exemple pour changer la fonte principale :

```latex
\usepackage[main={Source Sans Pro Light}]{fontes}
```


### Package `couleurs`

Définition centralisée des couleurs.

Trois couleurs de bases, `colorPrim`, `colorSec` et `colorTer` avec leur version allégée (`colorPrimLight` et `colorPrimLightest` par exemple).

Deux couleurs pour le fond et le texte avec `colorPage` et `colorText`.

### Package `competences`

#### Commandes

##### Définition manuelle

La commande `competence{}` est utilisée dans le texte de l'exercice, elle affiche la compétence *dans la marge gauche* ainsi qu'un cadre de quatre cases pour l'évaluation.

```latex
\competence{Résoudre des problèmes}
```

![Commande `competence`](images/competence.png "Commande `competence`")

##### <a name="code-competemces"></a>Utilisation de codes

Pour éviter de saisir les compétences à chaque fois, elles ont été stockées sour la forme d'un objet dans un fichier `referentielsCompetences.lua`. Un fonction Lua permet de récupérer
automatiquement les intitulés des compétences en fonction de leurs codes. Le fichier `referentielCompetencs.lua` doit être positionner dans le dossier `~/texmf/scripts/lualatex/` par exemple sous Linux
pour devenir accessible pour le package `competences`.

- Une commande `\CompetenceDuReferentiel{<code_competence>}` permet de retrouver une compétence de cycle 3 ou 4 en fonction de son code et de l'afficher dans le texte courant.

```latex
Blablabla \CompetenceDuReferentiel{c3NC2} blablabla\dots
```

- Définition de la commande `\marginCompetence`.
  
Utilisée face à un référentiel de compétences décrit dans un 


```latex
Blablabla \marginCompetence{c3NC2} blablabla\dots
```

![Commande `competence`](images/competence_ref.png "Commande `competence`")

### Package `boites`

Des environnements destinés à fabriquer des boîtes (utilisation du package [`tcolorbox`](https://www.ctan.org/pkg/tcolorbox))

#### Environnement `exo`

##### Description

Pour les exercices, avec compteur dédié.

```latex
\begin{exo}
  \input{\ExercicesPath/6Entiers01.tex}
\end{exo}
```

![Environnement `exo`](images/env_exo.png 'Environnement exo')

##### Options

Trois options sont disponibles~:

1. la première est un texte affiché à gauche du numéro de l'exercice (typiquement le nombre de points) ;
2. la deuxième est un code de la compétence (voir la partie sur les [comppétences automatisées](#code-competemces) )
3. la troisième est un texte correspondant au sujet de l'exercices

Si une option est laissée vide, rien n'est affiché à l'endroit correspondant.


```latex
\begin{exo}[4pts][c3EG3][Test]
  Babla 
  Babla 
  Babla 
  Babla 
  Babla 
  Babla
\end{exo}
```

![Environnement `exo`](images/env_exo_02.png 'Environnement exo')

#### Environnement `propriete`

Pour les propriétés, théorèmes, etc avec un compteur dédié. Par défaut, le compteur est marqué :

```latex
\begin{propriete}
Si deux droites sont perpendiculaires à une troisième droite alors elles sont parallèles.
\end{propriete}
```
![Environnement propriete avec compteur](images/env_prop_01.png)

En utilisant `propriete*`, on retire le compteur.

On peut ajouter un sous-titre avec une deuxième option.

```latex
\begin{propriete*}[\quad\normalfont \itshape Pythagore (sens direct)]
  Si un triangle est rectangle alors le carré de la longueur de son hypoténuse est égal à la somme des carrés des longueurs des côtés de l'angle droit.
\end{propriete*}
```
![Environnement propriete avec sous titre](images/env_prop_03.png)


Par défaut, le titre de la boîte est _Propriété_. Mais on peut en changer en passant une deuxième option.

```latex
\begin{propriete*}[][Théorème]
  Si deux droites sont parallèles à une même troisième droite alors ces deux droites sont parallèles.
\end{propriete*}
```

![Environnement propriete pour théorème](images/env_prop_02.png)

#### Environnement `definition`

Fonctionne à l'identique de l'environnement `propriete`.

```latex
\begin{definition}
  La \textbf{médiatrice} d'un segment est la droite qui passe par son milieu et qui lui est perpendiculaire. 1234567890
\end{definition}

\begin{definition}[\quad\normalfont \itshape (autre approche)]
  La \textbf{médiatrice} d'un segment est la droite formée par tous les points situés à égale distance des extrémités du segment.
\end{definition}
```
![Environnement definition](images/env_def_01.png)

![Environnement definition](images/env_def_02.png)

#### Environnement `boiteicone`

Pour dessiner des boîtes avec un logo. Quatre options sont disponibles :

* la première pour la couleur de la boîte ;
* le deuxième pour le titre de la boîte ;
* la troisième pour l'icône ;
* la quatrième et dernière pour la taille.

```latex
\begin{boiteicone}[colorTer][Aide][\faMedapps]
  Un test d'une aide ou d'une astuce.
\end{boiteicone}
```
![Environnement `boiteicone`](/images/env_boiteicone_01.png)

```latex
\begin{boiteicone}[colorPrim][][\faHeart]
  \begin{multicols}{2}
    \lipsum[1-4]
  \end{multicols}
\end{boiteicone}
```

![Environnement `boiteicone`](/images/env_boiteicone_02.png)

#### Environnement `boiteombre`

Pour dessiner une boîte simple avec une ombre.

```latex
\begin{boiteombre*}
  Voici une boite avec une ombre sans cadre de couleur grise par défaut.
\end{boiteombre*}
```
![Environnement `boiteombre`](/images/env_boiteombre_01.png)

Pour avoir un cadre, il faut juste enlever l'étoile. On peut préciser une couleur en option.

```latex
\begin{boiteombre}[NavyBlue]
  Voici une boite avec une ombre avec cadre de couleur \texttt{NavyBlue}.
\end{boiteombre}
```
![Environnement `boiteombre`](/images/env_boiteombre_02.png)

#### Environnement `progcalcul`

Pour écrire des programmes de calcul :

```latex
\begin{progcalcul}
  \item Choisir un nombre
  \item Multiplier par 7
  \item Soustraire 6 au résultat
\end{progcalcul}
```

La version `progcalcul*` retire la numérotation.

Pour remettre le compteur à zéro, faire 

```latex
\setcounter{progcalcul}{0}
```

#### Commandes

* `\Mhl` : pour surligner en mode mathématiques (sur le modèle de la commande `\hl` du package [`soul`](https://ctan.org/pkg/soul))
```latex
\begin{align*}
  A &= 23+4\times 7-\Mhl[green]{(10+2)}+5\\
  A &= 23 + \Mhl[green]{4 \times 7}-12+5\\
  A &= \Mhl[green]{23+28}-12+5 \\
  A &= \Mhl[green]{51-12}+5\\
  A &= \Mhl[green]{39+5}\\
  A &= 44
  \end{align*}
```
![Commande `\Mhl`](images/cmd_mhl.png)
* `\ConsigneQRCode` : pour déclarer un QRCode avec une consigne à droite. 
  Déclarer `\ConsigneQRCode[30mm]{<consigne>}{<url>}` pour avoir un QRCode de 30mm de côté.
```latex
\ConsigneQRCode{% consigne
      Les trois exercices sont en ligne.\\
      Ils traitent des priorités opératoires
    }{% lien
    https://shrtco.de/VB8BP9
    }
```
![Commande `\ConsigneQRCode`](images/cmd_consigneqrcode.png)
* `\Circled` : pour créer un caractère cerclé.
```latex
La module \Circled{1} est en premier.
Le module \Circled[colorSec]{A} vient après.
Le module \Circled[colorTer!20][colorTer]{s} vient ensuite.
```
![Commande `\Circled`](images/cmd_circled.png)
* `\Eu` pour afficher une somme en euros.
```latex
Une somme de \Eu{1200,45}.
Un prix (en \Eu{})\dots
```
![Commande `\Eu`](images/cmd_euro.png)

### Package `bandeaux`

Affiche des bandeaux pour les titres des documents.

#### Titres

Deux options `theme` et `niveau` sont prévues pour décrire le thème et le niveau.

L'option `couleur` permet de redéfinir la couleur de fond pour le logo (`colorPrim` par défaut).

#### Logos d'identification

Des options sont prévues pour typer le document en y associant un logo.

| Options | Intention | Logo |
| ------- | -------- | ---- |
| `exercices` | Pour les fiches d'exercices d'entraînement | ![Logo exercices](/images/logo_exercices.png) |
| `objectif` | Pour les fiches d'approche d'un concept | ![Logo objectif](/images/logo_objectif.png) |
| `methode` | Pour décrire des méthodes de calculs, de rédaction, etc. | ![Logo methode](/images/logo_methode.png) |
| `formative` | Pour les évaluations formatives | ![Logo formative](/images/logo_formative.png) |
| `finale` | Pour les évaluations sommatives | ![Logo finale](/images/logo_finale.png) |
| `memoire` | Pour les fiches de mémorisation active | ![Logo memoriser](/images/logo_memoire.png) |

#### Disposition

Des options permettent de positionner le bandeau soit sur le côté droite de la feuille, soit de manière plus traditionnelle en haut de la feuille.

<i class="fas fa-exclamation-triangle" aria-hidden="true"></i> Attention ! Il faut prévoir les marges en conséquence, notamment mentionner `twoside` 
lorsque le bandeau est positionner dans la partie gauche.

* Le bandeau est positionné sur le côté par défaut _et est répété_ sur les pages impaires.

```latex
\usepackage[objectif,niveau={SIXIÈME},theme={Nombres Entiers}]{bandeaux}
```

![Bandeau sur le côté](/images/page_cote.png)

* lorsque `top` est mentionné, le bandeau est positionné en haut de la feuille _et est répété_ sur les pages impaires.

```latex
\usepackage[methode,niveau={SIXIÈME},theme={Nombres Entiers},top,nobackground]{bandeaux}
```

![Bandeau sur le haut](/images/page_top.png)

* En ajoutant `nobackground`, le bandeau n'est présent que sur la première page...
* En ajoutant `nopagenumber`, il n'y a plus de numérotation pour les pages.

## Classes

Dans toutes les classes destinées aux élèves, je fais appel au magnifique package [`ProfCollege`](https://www.ctan.org/pkg/profcollege) de Christophe Poulain qui facilite grandement les choses dans l'écriture de contenus.